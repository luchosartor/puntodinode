#socket = io.connect('http://localhost')
video = document.getElementById('myVideo')
if navigator.mediaDevices and navigator.mediaDevices.getUserMedia
# Not adding `{ audio: true }` since we only want video now
  navigator.mediaDevices.getUserMedia(video: true).then (stream) ->
    video.src = window.URL.createObjectURL(stream)
    video.play()
    return
colors = new (tracking.ColorTracker)([
  'magenta'
  'cyan'
  'yellow'
])
colors.on 'track', (event) ->
  if event.data.length == 0
# No colors were detected in this frame.
  else
    horizontal = ''
    vertical = ''
    event.data.forEach (rect) ->
      if rect.x < video.clientWidth / 2
        horizontal = 'go left'
      else
        horizontal = 'go right'
      if rect.y < video.clientHeight / 2
        vertical = 'go up'
      else
        vertical = 'go down'
      console.log horizontal + ' and ' + vertical
      return
  return
tracking.track '#myVideo', colors