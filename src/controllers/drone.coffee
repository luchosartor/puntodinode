Cylon = require("cylon")
path = require("path")
ffmpeg = require('ffmpeg')
ffmpegPath = './bin/ffmpeg/ffmpeg'
#addon = require('../../build/Release/addon');


cacho = ->
  Cylon.robot(
    connections:
      opencv: adaptor: 'opencv'
      ardrone:
        adaptor: 'ardrone'
        port: '192.168.1.1'
    devices:
      drone:
        driver: 'ardrone'
        connection: 'ardrone'
      window:
        driver: 'window'
        connection: 'opencv'
    work: (my) ->
      @detect = false
      @image = null
      self = this
      my.drone.getPngStream().on 'data', (png) ->
        my.opencv.readImage png, (err, img) ->
          if err
            console.error err
          self.image = img
          if self.detect == false
            console.log(addon.hello())
            my.window.show img
          return
        return
  ).start()

module.exports =
  stream: cacho