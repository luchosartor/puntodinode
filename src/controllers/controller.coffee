Cylon = require('cylon')
module.exports =
  drone: require('./drone')
  jn: ->
    alert "JN IS THE BEST!"
  showStreaming: (req, res) ->
    res.render 'demo', {title: '.DI'}
  view: (req, res, next) ->
    res.render 'index', title: '.DI'
  takeOff: (req, res)->
    Cylon.robot(
      connections:
        ardrone:
          adaptor: 'ardrone'
          port: '192.168.1.1'
      devices:
        drone:
          driver: 'ardrone'
      work: (my) ->
        my.drone.takeoff()
        console.log "taking off"
        after 5.seconds(), ->
          my.drone.land()
          return
        after 10.seconds(), ->
          my.drone.stop()
          return
        return
    ).start()
    res.status(200).json({})

