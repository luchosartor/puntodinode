express = require('express')
router = express.Router()

controller = require('./controllers/controller')


### GET home page. ###

router.get '/', controller.view
router.get '/streaming', controller.showStreaming
router.get '/takeoff', controller.takeOff
router.get '/jn', controller.jn
#router.get '/hello', hello
router.get '/stream', controller.drone.stream


module.exports = router