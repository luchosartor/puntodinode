/**
 * Created by yankee on 16/11/16.
 */
tracking.ColorTracker.registerColor('purple', function(r, g, b) {
    var dx = r - 120;
    var dy = g - 60;
    var dz = b - 210;

    if ((b - g) >= 100 && (r - g) >= 60) {
        return true;
    }
    return dx * dx + dy * dy + dz * dz < 3500;
});

tracking.ColorTracker.registerColor('orange', function(r, g, b) {
    var dx = r - 255;
    var dy = g - 160;
    var dz = b - 0;

    if ((g - b) >= 120 && (r - b) >= 190) {
        return true;
    }
    return dx * dx + dy * dy + dz * dz < 3500;
});

tracking.ColorTracker.registerColor('white', function(r, g, b) {
    if (r > 220 && g > 220 && b > 220) {
        return true;
    }
    return false;
});

tracking.ColorTracker.registerColor('black', function(r, g, b) {
    if (r < 50 && g < 50 && b < 50) {
        return true;
    }
    return false;
});

tracking.ColorTracker.registerColor('green', function(r, g, b) {
    if (r < 50 && g > 200 && b < 50) {
        return true;
    }
    return false;
});
tracking.ColorTracker.registerColor('blue', function(r, g, b) {
    if (r < 50 && b > 200 && g < 50) {
        return true;
    }
    return false;
});
tracking.ColorTracker.registerColor('red', function(r, g, b) {
    if (g < 50 && r > 200 && b < 50) {
        return true;
    }
    return false;
});