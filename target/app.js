var express = require('express');
var path = require('path');
//var favicon = require('/img/drone-md.png');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//var cylon = require('cylon');
// var cylonArDrone = require('cylon-ardrone');

var routes = require('./routes');

var app = express();

var http = require('http').Server(app);
//var http = require('http').Server(app);
//var io = require('socket.io')(http);
//io.on('connection', function (socket) {
//  console.log("user connected");
//});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(cylon());
// app.use(cylonArDrone());
app.use(require('node-sass-middleware')({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true,
  sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  // var err = new Error('Not Found');
  // err.status = 404;
  // next(err);
  res.status(400);
  res.render('error404.jade', {title: '404: File Not Found'});
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.status(400);
    res.render('error.jade', {title: '500: Server Error'});
    // res.render('error', {
    //   message: err.message,
    //   error: err
    // });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.status(400);
  res.render('error.jade', {title: '500: Server Error'});
  // res.render('error', {
  //   message: err.message,
  //   error: {}
  // });
});

/*var io = require('socket.io-client');
var ss = require('socket.io-stream');
var fs = require('fs');

var socket = io.connect('/user');
var stream = ss.createStream();

ss(socket).emit('data', stream);
stream.pipe(fs.createWriteStream('file.txt'));*/

var io = require('socket.io')(http);

io.on('connection', function (socket) {
    console.log('a user connected');
    socket.on('disconnect', function () {
        console.log('user disconnected');
    });
    socket.on('chat message', function (msg) {
        console.log('message: ' + msg);
        var start = new Date().getTime();
        io.emit('chat message', "Start time:" + start);
        for (var i = 0; i < 10000; i++) {
            io.emit('chat message', i);
        }
        var end = new Date().getTime();
        io.emit('chat message', "End time:" + end);
        io.emit('chat message', "Total time:" + (end - start));
        io.emit('end', start);
    });
});

http.listen(2000, function(){
  console.log('listening on *:2000');
});



module.exports = app;
