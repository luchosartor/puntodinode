module.exports = (grunt) ->
	grunt.initConfig(
		pkg: grunt.file.readJSON('package.json')

		coffee:
			assets:
				options: { sourceMap: false }
				flatten: false
				expand: true
				cwd: 'src/assets/'
				src: ['*.coffee', '**/*.coffee']
				dest: 'target/public/'
				ext: '.js'
			other:
				options: { sourceMap: false }
				flatten: false
				expand: true
				cwd: 'src/'
				src: ['*.coffee', 'controllers/*.coffee', 'controllers/*/*.coffee', 'models/*.coffee', 'models/*/*.coffee', 'utils/*.coffee', 'utils/*/*.coffee']
				dest: 'target/'
				ext: '.js'

		sass:
			dist:
				files: [{
          expand: true
          cwd: 'src/assets/'
          src: ['*.scss', '**/*.scss']
          dest: 'target/public/'
          ext: '.css'
        }]

		copy:
			js:
				files: [{
          expand: true
          cwd: 'src/assets/'
          src: ['*.js', '**/*.js']
          dest: 'target/public/'
        }]

		watch:
			options: { cwd: 'src/' }
			coffee:
				files: ['*.coffee','**/*.coffee'],
				tasks: 'coffee'
			sass:
				files: ['*.sass', '**/*.sass', '*.scss', '**/*.scss'],
				tasks: 'sass'
			copy:
				files: ['*.js', '**/*.js'],
				tasks: 'copy'

	)

	grunt.loadNpmTasks 'grunt-contrib-coffee'
	grunt.loadNpmTasks 'grunt-contrib-sass'
	grunt.loadNpmTasks 'grunt-contrib-copy'
	grunt.loadNpmTasks 'grunt-contrib-watch'

	grunt.registerTask 'compile', ['coffee', 'sass', 'copy']
	grunt.registerTask 'default', ['coffee', 'sass', 'copy', 'watch']